from django.db import models
import datetime


# Create your models here.

class Status(models.Model):
    status = models.CharField(max_length = 300, default='status')
    time = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

    def __str__(self):
        return self.status