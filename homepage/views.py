from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm


# Create your views here.
def index(request):
    if request.method == 'POST':
        form = request.POST
        clear_form = StatusForm()

        status = Status.objects.create(status = form['status'])
        result = Status.objects.all().order_by('-id')

        response = {
            'result' : result,
            'form' : clear_form,
        }
        return render(request, 'index.html', response)

    else:
        form = StatusForm()
        result = Status.objects.all().order_by('-id')

        response = {
            'form' : form,
            'result' : result,
        }
        return render(request, 'index.html', response)

def delete_status(request, query_id):
    Status.objects.filter(id=query_id).delete()
    return redirect('homepage:index')

